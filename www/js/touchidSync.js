// wrapper convert callback API to ES6 promises 


/*
// Modulos o clases alternativa a class, import, export modules de ES6 
// prototype añade un miembro más (sin 'use strict' al asignar tambien vale )
var quiz = function() {}; //  or var Fingerprint = {};
Fingerprint.prototype.show = function (params, successCallback, errorCallback) {};
Fingerprint.prototype.isAvailable = function (successCallback, errorCallback) {};

// para espacio de nombres
var aesjs = {
    AES: AES,
    Counter: Counter,
    utils: {
        bbva: quiz,
        .....

 var quiz = function() {};   //  or var quiz = {}; 
 quiz.prototype.encriptar = function (data, word) {};       
 aesjs.utils.bbva.encriptar(data, Polymer.word);
 -----------------------
 var app = {
    initialize: function () {
    },
    onDeviceReady: function () {
    }
 };
*/


/* ES6 --> no compatible android browser --> https://kangax.github.io/compat-table/es6/    crome  y safari 11 /ios 11*/

var touchidSync = {

    isAvailable(){
        return new Promise(function(resolve,reject){
            window.plugins.touchid.isAvailable(resolve,reject);
        });
    },

    save(key, password, userAuthenticationRequired){
        return new Promise(function(resolve, reject,){
            window.plugins.touchid.save(key, password, userAuthenticationRequired, resolve,reject);
        });
    },

    verify(key, message){
        return new Promise(function(resolve,reject){
            window.plugins.touchid.verify(key, message, resolve,reject);
        });
    },

    delete(key){
        return new Promise(function(resolve,reject){
            window.plugins.touchid.delete(key, resolve,reject);
        });
    },

    has(key){
        return new Promise(function(resolve,reject){
            window.plugins.touchid.verify(key, resolve,reject);
        });
    }
};
