/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */



var listenerCallbackNFC = null;


var app = {
    // Application Constructor
    initialize() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    onDeviceReady() {
        this.receivedEvent('deviceready');
        AndroidFullScreen.immersiveMode(()=>console.log('immersiveMode'), (error)=>console.log(error));

		window.location="https://front-bank.appspot.com"


    },

    // Update DOM on a Received Event
    receivedEvent(id) {
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);
    },


    async showFingerprintDialog() {
        console.log('fingerprint');
        if (!window.plugins || !window.plugins.touchid) {
            console.log('no touchid plugin');
            return;
        }
        try {
            const available = await touchidSync.isAvailable()
            console.log(available);
            var serviceName = (available === "face") ? "Face ID" : "Touch ID";
            console.log("ServiceName: " + serviceName);
            var saved=await touchidSync.save("MyKey", "My Password");
            console.log(saved);
            var checked=await touchidSync.verify("MyKey", "Permitir acceso wallet");
            console.log(checked);
        } catch (e) {
            console.error(e);
        }
    },

    fingerprint() {
        console.log('fingerprint');
        if (!window.plugins || !window.plugins.touchid) {
            console.log('no touchid plugin');
            return;
        }
        if (window.plugins) {
            window.plugins.touchid.isAvailable(function (biometryType) {
                var serviceName = (biometryType === "face") ? "Face ID" : "Touch ID";
                window.plugins.touchid.has("MyKey", function () {
                    alert(serviceName + " avaialble and Password key available");
                }, function () {
                    alert(serviceName + " available but no Password Key available");
                });
            }, function (msg) {
                alert("no Touch ID available");
            });
        }

        if (window.plugins) {
            window.plugins.touchid.verify("MyKey", "My Message", function (password) {
                alert("Touch " + password);
            });
        }

        if (window.plugins) {
            window.plugins.touchid.save("MyKey", "My Password", true, function () {
                alert("Password saved");
            });
        }

        if (window.plugins) {
            window.plugins.touchid.delete("MyKey", function () {
                alert("Password key deleted");
            });
        }
    },


    listenerNFC() {
        console.log('listenerNFC');
        //var md = cordova.require("cordova/plugin_list").metadata; // ver plugins instalados
        //console.log(md); //if (md.phonegap-nfc) {} //if (window.nfc) {}
        if (!nfc) {
            console.log('no phonegap-nfc plugin');
            return;
        }
        // comprobar que no tiene asignado listener actualmente
        if (listenerCallbackNFC != null) {
            return;
        }

        // handler listener
        listenerCallbackNFC = function (event) {
            console.log('received message the tag contains: ', event.tag);
            var tagid = nfc.bytesToHexString(event.tag.id);
            console.log('decoded tag id', tagid);

            /*
            // conectar dependiendo de tipo que permita
            // "techTypes": ["android.nfc.tech.IsoDep", "android.nfc.tech.NfcA", "android.nfc.tech.Ndef"]
            var types= event.tag.techTypes;
            var type=types.find((e)=>e=='android.nfc.tech.Ndef') || types.find((e)=>e=='android.nfc.tech.IsoDep') || types[0];
    
            nfc.connect(type, 500).then(   // 5s timeout
                () => {
                    console.log('connected to', tagId);
                },
                (error) => console.log('connection failed', error)
            );
            */
        }

        // Registers an event listener for tags matching any tag type.
        // Read ANY formatted NFC Tags (establece listener de lecturas hasta remove, para capturar necesario primer plano app)
        nfc.addTagDiscoveredListener(
            listenerCallbackNFC,
            function () { // success callback
                console.log("Waiting for any tag");
            },
            function (error) { // error callback
                console.log("Error adding listener " + JSON.stringify(error));
            }
        );
    },

    removelistenerNFC() {
        console.log('removelistenerNFC');
        if (!listenerCallbackNFC) {
            return;
        }
        //nfc.removeNdefListener(callback, [onSuccess], [onFailure]);

        listenerCallbackNFC = null;

        nfc.removeTagDiscoveredListener(
            listenerCallbackNFC,
            () => {
                console.log('Successfully  remove listener');
            },
            (error) => {
                console.log('Error remove listener' + JSON.stringify(error));
            }
        );
    }






};


app.initialize();
